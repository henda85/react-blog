import { Layout } from "antd";
import { Router, Switch } from "react-router-dom";
import './App.css';
import { Nav } from "./components/Nav";
import { Home } from "./pages/Home";
    const { Header, Content, Footer } = Layout;

function App() {
    return (
        <Layout>
            <Header>
                <Nav />
            </Header>
            <Content>
                <Switch>
                    <Router  path="/" exact>
                        <Home/>
                    </Router>

                </Switch>
            </Content>
            <Footer>
            <p>copyright</p>
            </Footer>
        </Layout>
    );
    }

export default App;
